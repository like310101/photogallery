package com.like.photogallery.activity;

import android.content.Context;
import android.content.Intent;

import androidx.fragment.app.Fragment;

import com.like.photogallery.activity.SingleFragmentActivity;
import com.like.photogallery.fragment.PhotoGalleryFragment;

public class PhotoGalleryActivity extends SingleFragmentActivity {

    public static Intent newIntent(Context context) {
        return new Intent(context, PhotoGalleryActivity.class);
    }

    @Override
    protected Fragment createFrament() {
        return PhotoGalleryFragment.newInstance();
    }
}
