package com.like.photogallery.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;

import com.like.photogallery.R;

public abstract class SingleFragmentActivity extends AppCompatActivity {

    abstract protected Fragment createFrament();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_fragment);
        FragmentManager fm = getSupportFragmentManager();
        Fragment f = fm.findFragmentById(R.id.single_fragment_container);
        if (f == null) {
            f = createFrament();
            fm.beginTransaction().add(R.id.single_fragment_container, f).commit();
        }
    }
}
